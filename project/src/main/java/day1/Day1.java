package day1;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Day1 {
    static class LevelInfo {
        Integer firstBasement, finalLevel;

        LevelInfo(Integer fb, Integer fl) {
            firstBasement = fb;
            finalLevel = fl;
        }

        @Override
        public String toString() {
            StringBuilder str;

            str = new StringBuilder();
            str.append("Basement first reached at: " + firstBasement + ", the final level is: " + finalLevel);
            return str.toString();
        }
    }

    public static LevelInfo getLevelInfo(BufferedInputStream in) throws IOException {
        LevelInfo levelInfo;
        int c;
        int finalLevel = 0;
        Integer firstBasement = null;
        for (int i = 1; (c = in.read()) != -1; i++) {
            if (c == '(') {
                finalLevel++;
            } else if (c == ')') {
                finalLevel--;
            }
            if (finalLevel == -1 && firstBasement == null) {
                firstBasement = i;
            }
        }
        levelInfo = new LevelInfo(firstBasement, finalLevel);
        return levelInfo;
    }

    public static void main(String[] args) {
        BufferedInputStream inputStream;

        try {
            inputStream = new BufferedInputStream(new FileInputStream("src/main/java/day1/input"));
            System.out.println(getLevelInfo(inputStream).toString());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
}
