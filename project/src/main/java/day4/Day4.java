package day4;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Day4 {
    public static String getMD5(String in) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");

        byte[] messageDigest = md.digest(in.getBytes());

        BigInteger no = new BigInteger(1, messageDigest);

        StringBuilder hashText = new StringBuilder(no.toString(16));
        while (hashText.length() < 32) {
            hashText.insert(0, '0');
        }
        return hashText.toString();
    }

    public static boolean firstNZero(String in, int n) {
        boolean ret = true;
        for (int i = 0; i < n; i++) {
            ret &= in.charAt(i) == '0';
        }
        return ret;
    }

    public static int numberRequiredForNumZeroKey(String in, int numZero) throws NoSuchAlgorithmException {
        StringBuilder s = new StringBuilder(in);
        for (Integer i = 1;; i++) {
            // Append the number to the end of the string
            s.append(i.toString());
            if (firstNZero(getMD5(s.toString()), numZero)) {
                return i;
            }
            // Remove the number from the end of the string for next loop
            s.delete(s.length() - i.toString().length(), s.length());
        }
    }

    public static void main(String[] args) {
        String s = "iwrupvqb";
        try {
            System.out.println("test: " + getMD5("abcdef609043"));
            System.out.println("Number Required for a key starting with 5 zeros with \"" + s + "\" = "
                    + numberRequiredForNumZeroKey(s, 5));
            System.out.println("Number Required for a key starting with 6 zeros with \"" + s + "\" = "
                    + numberRequiredForNumZeroKey(s, 6));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.err.println("FAILED TO USE MD5 ALGORITHM");
        }
    }
}
