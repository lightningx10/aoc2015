package day8;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Day8 {
    public static void read(Scanner in) {
        int numCode = 0, numMem = 0;
        while (in.hasNextLine()) {
            String l = in.nextLine();
            // System.out.println(l);
            numCode += l.length();
            numMem += l.length() - 2;
            for (char c : l.toCharArray()) {
                if (c == '\\') {
                    numMem--;
                }
            }
            if (l.contains("\\x")) {
                for (int i = 0; i < l.length() - 1; i++) {
                    if (l.substring(i, i + 2).equals("\\x")) {
                        numMem -= 2;
                    }
                }
            }
        }
        System.out.println("Number of chars in code: " + numCode + " Number of chars in memory: " + numMem);
        System.out.println("Therefore the answer is: " + (numCode - numMem));
    }

    public static void altRead(Scanner in) {
        int numCode = 0, numMem = 0;
        while (in.hasNextLine()) {
            String l = in.nextLine();
            numCode += l.length();
            numMem += l.length() - 2;
            for (int i = 0; i < l.length(); i++) {
                if (l.charAt(i) == '\\') {
                    switch (l.charAt(i + 1)) {
                    case '\\':
                        numMem--;
                        break;
                    case '\"':
                        numMem--;
                        break;
                    case 'x':
                        numMem -= 3;
                        break;
                    }
                }
            }
        }
        System.out.println("Number of chars in code: " + numCode + " Number of chars in memory: " + numMem);
        System.out.println("Therefore the answer is: " + (numCode - numMem));
    }

    public static void main(String[] args) {
        Scanner in;

        try {
            in = new Scanner(new FileInputStream("src/main/java/day8/tinput"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        altRead(in);
        System.out.println("This should have been code = 23, mem = 11, ans = 12");

        try {
            in = new Scanner(new FileInputStream("src/main/java/day8/input"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        altRead(in);

        in.close();
        return;
    }
}
