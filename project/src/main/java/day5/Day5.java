package day5;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day5 {
    static boolean isGoodString(String s) {
        boolean containsDouble = false;
        int numVowel = 0;

        for (int i = 0; i < s.length(); i++) {
            if (i != s.length() - 1) {
                switch (s.substring(i, i + 2)) {
                case "ab":
                case "cd":
                case "pq":
                case "xy":
                    return false;
                default:
                    break;
                }
                containsDouble |= s.charAt(i) == s.charAt(i + 1);
            }
            switch (s.charAt(i)) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                numVowel++;
                break;
            default:
                break;
            }
        }
        return containsDouble && numVowel >= 3;
    }

    static boolean isNewGoodString(String s) {
        boolean rule1 = false; // contains a pair of letters that repeat.
        boolean rule2 = false; // at least one letter which repeats with exactly one letter between it and the
                               // next.

        // rule 1:
        for (int i = 0; i < s.length() - 1; i++) {
            String pair = s.substring(i, i + 2);
            StringBuilder sb = new StringBuilder(s);
            // StringBuilder sb1 = new StringBuilder(s);
            // StringBuilder sb2 = new StringBuilder(s);
            // sb1.delete(i, s.length());
            // sb2.delete(0, i + 2);
            // rule1 |= sb1.toString().contains(pair) || sb2.toString().contains(pair);
            sb.delete(i, i + 2);
            rule1 |= sb.toString().contains(pair);

            // rule 2:
            if (i < s.length() - 2) {
                rule2 |= s.charAt(i) == s.charAt(i + 2);
            }
        }

        return rule1 && rule2;
    }

    public static void main(String[] args) {
        Scanner in;
        int numGood, numNewGood;

        // "Tests":
        System.out.println("ugknbfddgicrmopn is a " + (isGoodString("ugknbfddgicrmopn") ? "good" : "bad") + " string");
        System.out.println("aaa is a " + (isGoodString("aaa") ? "good" : "bad") + " string");
        System.out.println("jchzalrnumimnmhp is a " + (isGoodString("jchzalrnumimnmhp") ? "good" : "bad") + " string");
        System.out.println("haegwjzuvuyypxyu is a " + (isGoodString("haegwjzuvuyypxyu") ? "good" : "bad") + " string");
        System.out.println("dvszwmarrgswjxmb is a " + (isGoodString("dvszwmarrgswjxmb") ? "good" : "bad") + " string");
        System.out.println(
                "qjhvhtzxzqqjkmpb is a " + (isNewGoodString("qjhvhtzxzqqjkmpb") ? "new good" : "new bad") + " string");
        System.out.println("xxyxx is a " + (isNewGoodString("xxyxx") ? "new good" : "new bad") + " string");
        System.out.println(
                "uurcxstgmygtbstg is a " + (isNewGoodString("uurcxstgmygtbstg") ? "new good" : "new bad") + " string");
        System.out.println(
                "ieodomkazucvgmuy is a " + (isNewGoodString("ieodomkazucvgmuy") ? "new good" : "new bad") + " string");
        // Actual task:
        try {
            in = new Scanner(new FileInputStream("src/main/java/day5/input"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        numGood = numNewGood = 0;
        System.out.println("Enter input:");
        while (in.hasNextLine()) {
            String l = in.nextLine();
            if (isGoodString(l)) {
                numGood++;
            }
            if (isNewGoodString(l)) {
                numNewGood++;
            }
        }

        System.out.println("The number of good lines is " + numGood);
        // Currently reports 4 false positives. Was not able to work out what :(
        System.out.println("The number of new good lines is " + numNewGood);

        in.close();
        return;
    }
}
