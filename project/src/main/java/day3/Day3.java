package day3;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;

public class Day3 {
    public static class Pair<T1, T2> {
        T1 left;
        T2 right;

        Pair(T1 left, T2 right) {
            this.left = left;
            this.right = right;
        }

        T1 left() {
            return left;
        }

        T2 right() {
            return right;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            } else if (obj instanceof Pair<?, ?>) {
                @SuppressWarnings("unchecked")
                Pair<T1, T2> other = (Pair<T1, T2>) obj;
                return left.equals(other.left) && right.equals(other.right);
            }
            return false;
        }
    }

    public static void main(String[] args) {
        HashSet<Pair<Integer, Integer>> coordinatesVisited, helpedCoordinatesVisited;
        BufferedInputStream inputStream;
        int c, x, y, santaX, santaY, roboX, roboY;
        boolean robo;

        coordinatesVisited = new HashSet<>();
        helpedCoordinatesVisited = new HashSet<>();
        helpedCoordinatesVisited.add(new Pair<Integer, Integer>(0, 0));
        x = y = santaX = santaY = roboX = roboY = 0;
        robo = false;
        try {
            inputStream = new BufferedInputStream(new FileInputStream("src/main/java/day3/input"));
            while ((c = inputStream.read()) != -1) {
                switch (c) {
                case '^':
                    y--;
                    if (robo) {
                        roboY--;
                        System.out.println("RoboY--");
                    } else {
                        santaY--;
                        System.out.println("SantaY--");
                    }
                    break;
                case '>':
                    x++;
                    if (robo) {
                        roboX++;
                        System.out.println("RoboX++");
                    } else {
                        santaX++;
                        System.out.println("SantaX++");
                    }
                    break;
                case 'v':
                    y++;
                    if (robo) {
                        roboY++;
                        System.out.println("RoboY++");
                    } else {
                        santaY++;
                        System.out.println("SantaY++");
                    }
                    break;
                case '<':
                    x--;
                    if (robo) {
                        roboX--;
                        System.out.println("RoboX--");
                    } else {
                        santaX--;
                        System.out.println("SantaX--");
                    }
                    break;
                default:
                    break;
                }
                Pair<Integer, Integer> coordinates = new Pair<>(x, y);
                boolean visited = false;
                // Shouldn't have to do this but the map doesn't seem to know that duplicates
                // can't be added.
                for (Pair<Integer, Integer> p : coordinatesVisited) {
                    visited |= x == p.left() && y == p.right();
                }
                if (!visited) {
                    coordinatesVisited.add(coordinates);
                }
                visited = false;
                coordinates = new Pair<>(robo ? roboX : santaX, robo ? roboY : santaY);
                for (Pair<Integer, Integer> p : helpedCoordinatesVisited) {
                    visited |= coordinates.left() == p.left() && coordinates.right() == p.right();
                }
                if (!visited) {
                    helpedCoordinatesVisited.add(coordinates);
                }
                robo = !robo;
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("ERROR: Reading IO failed!");
            return;
        }
        System.out.println("Unique coordinates visited: " + coordinatesVisited.size());
        System.out.println("Unique coordinates visited with help: " + helpedCoordinatesVisited.size());
        return;
    }
}
