package day7;

import day7.Day7.Gate.Type;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/*
 * Warning, this solution has so much recursion that it results in a calculation time in the many minutes,
 * NOT a good solution at all.
 *
 */

public class Day7 {
    public static class Input {
        Short value;
        Gate wire1, wire2;

        Input(short value) {
            this.value = value;
            wire1 = wire2 = null;
        }

        Input(Gate wire) {
            wire1 = wire2 = wire;
            this.value = null;
        }

        Input(Gate wire1, Gate wire2) {
            this.wire1 = wire1;
            this.wire2 = wire2;
            this.value = null;
        }
    }

    public static class Gate {
        enum Type {
            AND, OR, LSHIFT, RSHIFT, NOT, WIRE, NONE
        }

        Input input;
        Type type;
        Short shift;

        Gate() {
            this.type = Type.NONE;
            input = null;
            shift = null;
        }

        public void makeEqual(Gate other) {
            input = other.input;
            type = other.type;
            shift = other.shift;
        }

        Gate(Type type, Gate input1, Gate input2) {
            if (type != Type.AND && type != Type.OR) {
                System.err.println("AND/OR Gate constructor given wrong type");
                return;
            }
            this.input = new Input(input1, input2);
            this.type = type;
            shift = null;
        }

        Gate(Type type, Gate input) {
            if (type != Type.NOT && type != Type.WIRE) {
                System.err.println("NOT/WIRE Gate constructor given wrong type");
                return;
            }
            this.input = new Input(input);
            this.type = type;
            shift = null;
        }

        Gate(Type type, Input input) {
            if (type != Type.WIRE) {
                System.err.println("WIRE Gate constructor given wrong type");
                return;
            }
            this.input = input;
            this.type = type;
            shift = null;
        }

        Gate(Type type, Gate input, short shift) {
            if (type != Type.LSHIFT && type != Type.RSHIFT) {
                System.err.println("LSHIFT/RSHIFT Gate constructor given wrong type");
                return;
            }
            this.input = new Input(input);
            this.type = type;
            this.shift = shift;
        }

        Short getOutput(HashMap<String, Short> valMap) {
            if (valMap.containsKey(((Short) (short) this.hashCode()).toString())) {
                return valMap.get(((Short) (short) this.hashCode()).toString());
            }
            Short i1, i2, ret;
            if (this.input == null) {
                return null;
            }
            i1 = this.input.wire1 == null ? null : this.input.wire1.getOutput(valMap);
            i2 = this.input.wire2 == null ? null : this.input.wire2.getOutput(valMap);
            ret = null;
            switch (type) {
            case AND:
                ret = (i1 == null || i2 == null) ? null : (short) (i1 & i2);
                break;
            case OR:
                ret = (i1 == null || i2 == null) ? null : (short) (i1 | i2);
                break;
            case LSHIFT:
                ret = (i1 == null) ? null : (short) (i1 << shift);
                break;
            case RSHIFT:
                ret = (i1 == null) ? null : (short) (i1 >> shift);
                break;
            case NOT:
                ret = (i1 == null) ? null : (short) ~i1;
                break;
            case WIRE:
                ret = this.input.value == null ? i1 : this.input.value;
                break;
            case NONE:
                ret = null;
                break;
            }
            valMap.put(((Short) (short) this.hashCode()).toString(), ret);
            return ret;
        }
    }

    static class CircuitBoard {
        HashMap<String, Gate> board;

        CircuitBoard() {
            board = new HashMap<>();
        }

        void addWire(String name, int val) {
            if (board.keySet().contains(name)) {
                if (board.get(name).type != Type.NONE) {
                    System.err.println("WARNING: REDEFINING A SET GATE!");
                } else {
                    board.get(name).makeEqual(new Gate(Type.WIRE, new Input((short) val)));
                }
            } else {
                board.put(name, new Gate(Gate.Type.WIRE, new Input((short) val)));
            }
        }

        void addWire(String name, String gate) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate)) {
                board.put(gate, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.WIRE, board.get(gate)));
            } else {
                board.put(name, new Gate(Type.WIRE, board.get(gate)));
            }
        }

        void addAndGate(String name, String gate1, String gate2) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate1)) {
                board.put(gate1, new Gate());
            }
            if (!board.keySet().contains(gate2)) {
                board.put(gate2, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.AND, board.get(gate1), board.get(gate2)));
            } else {
                board.put(name, new Gate(Type.AND, board.get(gate1), board.get(gate2)));
            }
        }

        void addAndGate(String name, int gate1, String gate2) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate2)) {
                board.put(gate2, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name)
                        .makeEqual(new Gate(Type.AND, new Gate(Type.WIRE, new Input((short) gate1)), board.get(gate2)));
            } else {
                board.put(name, new Gate(Type.AND, new Gate(Type.WIRE, new Input((short) gate1)), board.get(gate2)));
            }
        }

        void addOrGate(String name, String gate1, String gate2) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate1)) {
                board.put(gate1, new Gate());
            }
            if (!board.keySet().contains(gate2)) {
                board.put(gate2, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.OR, board.get(gate1), board.get(gate2)));
            } else {
                board.put(name, new Gate(Type.OR, board.get(gate1), board.get(gate2)));
            }
        }

        void addOrGate(String name, int gate1, String gate2) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate2)) {
                board.put(gate2, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name)
                        .makeEqual(new Gate(Type.OR, new Gate(Type.WIRE, new Input((short) gate1)), board.get(gate2)));
            } else {
                board.put(name, new Gate(Type.OR, new Gate(Type.WIRE, new Input((short) gate1)), board.get(gate2)));
            }
        }

        void addNotGate(String name, String gate) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate)) {
                board.put(gate, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.NOT, board.get(gate)));
            } else {
                board.put(name, new Gate(Type.NOT, board.get(gate)));
            }
        }

        void addLShiftGate(String name, String gate, int shift) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate)) {
                board.put(gate, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.LSHIFT, board.get(gate), (short) shift));
            } else {
                board.put(name, new Gate(Type.LSHIFT, board.get(gate), (short) shift));
            }
        }

        void addRShiftGate(String name, String gate, int shift) {
            if (board.keySet().contains(name) && board.get(name).type != Type.NONE) {
                System.err.println("WARNING: REDEFINING A SET GATE!");
            }
            if (!board.keySet().contains(gate)) {
                board.put(gate, new Gate());
            }
            if (board.keySet().contains(name)) {
                board.get(name).makeEqual(new Gate(Type.RSHIFT, board.get(gate), (short) shift));
            } else {
                board.put(name, new Gate(Type.RSHIFT, board.get(gate), (short) shift));
            }
        }

        public HashMap<String, Gate> getBoard() {
            return board;
        }
    }

    public static boolean isInt(String s) {
        boolean isInt = true;
        for (char c : s.toCharArray()) {
            isInt &= c >= '0' && c <= '9';
        }
        return isInt;
    }

    public static void getInput(Scanner in, CircuitBoard circuitBoard) {
        while (in.hasNextLine()) {
            String l = in.nextLine();
            String arg = "";
            for (Type t : Type.values()) {
                if (l.contains(t.toString())) {
                    arg = t.toString();
                }
            }
            String n1 = null, n2 = null, n3 = null;

            if (arg.equals("")) {
                System.out.println("Setting a variable");
                n1 = l.substring(0, l.indexOf(' '));
                n2 = l.substring(l.indexOf('>') + 2); //
                // System.out.println("n1: " + n1 + " n2: " + n2);
                if (isInt(n1)) {
                    circuitBoard.addWire(n2, Integer.valueOf(n1));
                } else {
                    circuitBoard.addWire(n2, n1);
                }
            }

            switch (arg) {
            case "AND":
            case "OR":
            case "LSHIFT":
            case "RSHIFT":
                n1 = l.substring(0, l.indexOf(' '));
                n2 = l.substring(l.indexOf(' ', l.indexOf(arg)) + 1);
                n3 = n2.substring(n2.indexOf('>') + 2);
                n2 = n2.substring(0, n2.indexOf(' '));
                // System.out.println("n1: " + n1 + " n2: " + n2 + " n3: " + n3);
                switch (arg) {
                case "AND":
                    if (isInt(n1)) {
                        circuitBoard.addAndGate(n3, Integer.valueOf(n1), n2);
                    } else if (isInt(n2)) {
                        circuitBoard.addAndGate(n3, Integer.valueOf(n2), n1);
                    } else {
                        circuitBoard.addAndGate(n3, n1, n2);
                    }
                    break;
                case "OR":
                    if (isInt(n1)) {
                        circuitBoard.addOrGate(n3, Integer.valueOf(n1), n2);
                    } else if (isInt(n2)) {
                        circuitBoard.addOrGate(n3, Integer.valueOf(n2), n1);
                    } else {
                        circuitBoard.addOrGate(n3, n1, n2);
                    }
                    break;
                case "LSHIFT":
                    circuitBoard.addLShiftGate(n3, n1, Integer.valueOf(n2));
                    break;
                case "RSHIFT":
                    circuitBoard.addRShiftGate(n3, n1, Integer.valueOf(n2));
                    break;
                }
                break;
            case "NOT":
                n1 = l.substring(l.indexOf(' ') + 1);
                n2 = n1.substring(n1.indexOf('>') + 2);
                n1 = n1.substring(0, n1.indexOf(' '));
                // System.out.println("n1: " + n1 + " n2: " + n2);
                circuitBoard.addNotGate(n2, n1);
            default:
                break;
            }
        }
    }

    public static void main(String[] args) {
        Scanner in;
        CircuitBoard testCircuitBoard, circuitBoard;
        HashMap<String, Short> testValMap, valMap;

        // tests:
        testCircuitBoard = new CircuitBoard();
        testValMap = new HashMap<>();
        testCircuitBoard.addWire("x", 123);
        testCircuitBoard.addWire("y", 456);
        testCircuitBoard.addAndGate("d", "x", "y");
        testCircuitBoard.addOrGate("e", "x", "y");
        testCircuitBoard.addLShiftGate("f", "x", 2);
        testCircuitBoard.addRShiftGate("g", "y", 2);
        testCircuitBoard.addNotGate("h", "x");
        testCircuitBoard.addNotGate("i", "y");

        System.out.println("Tests:");
        for (String s : testCircuitBoard.getBoard().keySet()) {
            System.out
                    .println(s + ": " + Short.toUnsignedInt(testCircuitBoard.getBoard().get(s).getOutput(testValMap)));
        }

        System.out.println("Enter input:");
        try {
            in = new Scanner(new FileInputStream("src/main/java/day7/input"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        circuitBoard = new CircuitBoard();

        getInput(in, circuitBoard);

        Short n;
        valMap = new HashMap<>();

        System.out.println("Variable Values:");
        for (String s : circuitBoard.getBoard().keySet()) {
            n = circuitBoard.getBoard().get(s).getOutput(valMap);
            System.out.println(s + ": " + (n == null ? "null" : Short.toUnsignedInt(n)));
        }
        n = circuitBoard.getBoard().get("a").getOutput(valMap);
        System.out.println("Value of a: " + (n == null ? "null" : Short.toUnsignedInt(n)));

        System.out.println("Next part:");
        circuitBoard.getBoard().get("b").input.value = (short) 956;
        circuitBoard.getBoard().get("b").input.wire1 = null;
        circuitBoard.getBoard().get("b").input.wire2 = null;

        System.out.println("New Variable Values:");
        for (String s : circuitBoard.getBoard().keySet()) {
            n = circuitBoard.getBoard().get(s).getOutput(valMap);
            System.out.println(s + ": " + (n == null ? "null" : Short.toUnsignedInt(n)));
        }
        n = circuitBoard.getBoard().get("a").getOutput(valMap);
        System.out.println("Value of a: " + (n == null ? "null" : Short.toUnsignedInt(n)));

        in.close();
        return;
    }
}
