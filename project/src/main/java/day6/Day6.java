package day6;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Day6 {
    public static class Coordinate {
        private int x, y;

        Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        Coordinate(String in) {
            // Expecting input of type "(x, y)"
            this.x = Integer.valueOf(in.substring(1, in.indexOf(',')));
            this.y = Integer.valueOf(in.substring(in.indexOf(',') + 1, in.indexOf(')')));
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public void setX(int x) {
            this.x = x;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return "(" + this.x + ", " + this.y + ")";
        }
    }

    public static class Range {
        private Coordinate start, end;

        Range(Coordinate start, Coordinate end) {
            this.start = start;
            this.end = end;
        }

        public Coordinate getStart() {
            return start;
        }

        public Coordinate getEnd() {
            return end;
        }

        public void setStart(Coordinate start) {
            this.start = start;
        }

        public void setEnd(Coordinate end) {
            this.end = end;
        }

        public void toggleRange(boolean[][] field) {
            for (int x = start.x; x <= end.x; x++) {
                for (int y = start.y; y <= end.y; y++) {
                    field[x][y] = !field[x][y];
                }
            }
        }

        public void toggleRange(int[][] field) {
            for (int x = start.x; x <= end.x; x++) {
                for (int y = start.y; y <= end.y; y++) {
                    field[x][y] += 2;
                }
            }
        }

        public void setRange(boolean[][] field, boolean set) {
            for (int x = start.x; x <= end.x; x++) {
                for (int y = start.y; y <= end.y; y++) {
                    field[x][y] = set;
                }
            }
        }

        public void setRange(int[][] field, boolean set) {
            for (int x = start.x; x <= end.x; x++) {
                for (int y = start.y; y <= end.y; y++) {
                    field[x][y] += set ? 1 : -1;
                    if (field[x][y] < 0) {
                        field[x][y] = 0;
                    }
                }
            }
        }
    }

    static boolean isNum(char c) {
        return c >= '0' && c <= '9';
    }

    static Coordinate getNextCoordinate(String s, int startingIndex) {
        Coordinate ret;
        int start, end;

        for (start = startingIndex; !isNum(s.charAt(start)) && start < s.length(); start++)
            ;
        end = s.indexOf(' ', start);
        if (end == -1) {
            end = s.length();
        }
        ret = new Coordinate("(" + s.substring(start, end) + ")");

        return ret;
    }

    public static void main(String[] args) {
        Scanner in;
        boolean[][] lights;
        int[][] brightLights;
        int numLit, totalBrightness;

        try {
            in = new Scanner(new FileInputStream("src/main/java/day6/input"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        lights = new boolean[1000][1000];
        brightLights = new int[1000][1000];

        // tests:
        Coordinate testCoordinate = new Coordinate("(69,420)");
        System.out.println("Test Coordinate: " + testCoordinate);

        while (in.hasNextLine()) {
            String l = in.nextLine();
            int firstArgEnd = l.indexOf(' ');
            Range r;
            switch (l.substring(0, firstArgEnd)) {
            case "turn":
                int nextArgEnd = l.indexOf(' ', firstArgEnd + 1);
                boolean on = l.substring(firstArgEnd + 1, nextArgEnd).equals("on");
                // System.out.println(l.subSequence(firstArgEnd + 1, nextArgEnd));
                r = new Range(getNextCoordinate(l, 0), getNextCoordinate(l, l.indexOf(' ', nextArgEnd + 1)));
                // System.out.println("Start: " + r.getStart() + " End: " + r.getEnd() + " On: "
                // + on);
                r.setRange(lights, on);
                r.setRange(brightLights, on);
                break;
            case "toggle":
                r = new Range(getNextCoordinate(l, 0), getNextCoordinate(l, l.indexOf(' ', firstArgEnd + 1)));
                // System.out.println("Start: " + r.getStart() + " End: " + r.getEnd());
                r.toggleRange(lights);
                r.toggleRange(brightLights);
                break;
            default:
                break;
            }
        }

        numLit = 0;
        totalBrightness = 0;
        for (boolean[] row : lights) {
            for (boolean light : row) {
                if (light) {
                    numLit++;
                }
            }
        }
        for (int[] row : brightLights) {
            for (int brightness : row) {
                totalBrightness += brightness;
            }
        }

        System.out.println("The number lit is " + numLit);
        System.out.println("The total brightness is " + totalBrightness);

        in.close();
        return;
    }
}
