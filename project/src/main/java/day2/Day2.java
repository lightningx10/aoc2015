package day2;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day2 {
  static class Present {
    int length, width, height;

    Present(int length, int width, int height) {
      this.length = length;
      this.width = width;
      this.height = height;
    }

    public int getSurfaceArea() {
      return (length * width * 2) + (width * height * 2) + (height * length * 2);
    }

    public int getSlackArea() {
      return (length < width) ? ((width < height) ? length * width : length * height)
          : ((length < height) ? length * width : width * height);
    }

    public int getRibbonLength() {
      return ((length < width) ? ((width < height) ? 2 * (length + width) : 2 * (length + height))
          : ((length < height) ? 2 * (length + width) : 2 * (width + height))) + length * width * height;
    }

    @Override
    public String toString() {
      return "Present: Length = " + length + " Width = " + width + " Height = " + height;
    }
  }

  static ArrayList<Present> getPresents(Scanner in) throws IOException {
    ArrayList<Present> ret;
    ArrayList<Integer> inCache, argCache;

    ret = new ArrayList<>();
    inCache = new ArrayList<>();
    argCache = new ArrayList<>();

    while (in.hasNextLine()) {
      String ln = in.nextLine();
      for (char c : ln.toCharArray()) {
        if (c >= '0' && c <= '9') {
          inCache.add(c - '0');
        } else {
          int addInt = 0;
          for (int i = 0; i < inCache.size(); i++) {
            addInt += inCache.get(i) * Math.pow(10, inCache.size() - 1 - i);
          }
          argCache.add(addInt);
          inCache.clear();
        }
      }
      if (inCache.size() != 0) {
        int addInt = 0;
        for (int i = 0; i < inCache.size(); i++) {
          addInt += inCache.get(i) * Math.pow(10, inCache.size() - 1 - i);
        }
        argCache.add(addInt);
        inCache.clear();
      }
      ret.add(new Present(argCache.get(0), argCache.get(1), argCache.get(2)));
      argCache.clear();
    }

    return ret;
  }

  public static void main(String[] args) {
    ArrayList<Present> presents;
    Scanner in;
    int totalSurfaceArea, totalRibbonLength;

    try {
      in = new Scanner(new FileInputStream("src/main/java/day2/input"));
      presents = getPresents(in);
    } catch (IOException e) {
      e.printStackTrace();
      return;
    }

    totalSurfaceArea = 0;
    totalRibbonLength = 0;
    for (Present p : presents) {
      totalSurfaceArea += p.getSurfaceArea() + p.getSlackArea();
      totalRibbonLength += p.getRibbonLength();
    }
    System.out.println("The total surface area is: " + totalSurfaceArea);
    System.out.println("The total ribbon length is: " + totalRibbonLength);
  }
}
